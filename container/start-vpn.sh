#!/bin/bash
# Needed for openvpn
mkdir -p /run/openvpn
mkdir -p /dev/net
mknod /dev/net/tun c 10 200

# Ensure routing is on
sysctl -w net.ipv4.ip_forward=1
# Firewall
# Allow traffic initiated from VPN to access "the world"
iptables -F
# Allow traffic initiated from VPN to access "the world"
iptables -I FORWARD -i tun0 -o eth0 -m conntrack --ctstate NEW -j ACCEPT
# Masquerade traffic from VPN to "the world" -- done in the nat table
iptables -t nat -I POSTROUTING -o eth0 -j MASQUERADE
OVPNCFGFILE=$(ls -d /data/* | grep -i '\.ovpn' | sort | head -n 1)

# Needed permissions - especially for stupid stuff like CRL
chown -R nobody:nogroup /data
chmod -R 700 /data

# While loop who will test conectivity and kill openvpn if no internet or DNS (ping-based)
GRACEPERIOD=0
NETDOWNCOUNT=0
sleep 60 && while :
do
        if ping -q -c 1 -W 2 google.com >/dev/null; then
                echo "$(date) The network is up" > /data/netmon.log
                sleep 1
                ((GRACEPERIOD++))
                if [ "$GRACEPERIOD" -gt 10 ]; then
                        NETDOWNCOUNT=0
                        GRACEPERIOD=0
                fi
        else
                echo "$(date) The network is down, retrying..." >> /data/netmon.log
                ((NETDOWNCOUNT++))
                if [ "$NETDOWNCOUNT" -gt 5 ]; then
                        echo "$(date) Network down for more than 5 seconds. exiting." >> /data/networkrestet.log
                        pkill openvpn
                        exit 1
                fi
                sleep 1
        fi
done &

# Run actual OpenVPN
openvpn --writepid /run/openvpn/openvpn.pid --cd /data --config $OVPNCFGFILE --script-security 2 --up /etc/openvpn/up.sh --down /etc/openvpn/down.sh
